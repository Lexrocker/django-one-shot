from django.shortcuts import redirect, render
from django.urls import reverse_lazy

from django.views.generic import ListView, DetailView  # put these imports for all class views in notes
from django.views.generic.edit import CreateView

from todos.models import TodoList, TodoItem

# Create your views here.

class TodoListView(ListView):
    model = TodoList    # put this in notes, for class views is model not context
    template_name = "todos/list.html" # also put this in notes for funsies
    context_object_name = "todo_list" # change the default context name to this



class TodoDetailView(DetailView):
    model = TodoList
    template_name = "todos/detail.html"
    context_object_name = "todo_detail"


class CreateNewListView(CreateView):
    model = TodoList
    template_name = "todos/new.html"
    context_object_name = "create_todolist"
    fields = ['name']
    success_url = reverse_lazy("todos:show_todolist")

    def get_success_url(self):
        return reverse_lazy("todos:show_todolist", kwargs={'pk': self.object.pk})

