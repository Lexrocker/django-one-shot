from django.contrib import admin
from todos.models import TodoList, TodoItem   # add this to notes - specifiy locaiton

# Register your models here.

class TodoListAdmin(admin.ModelAdmin):  # add this to notes
    pass

class TodoItemAdmin(admin.ModelAdmin):  # add this to notes
    pass

admin.site.register(TodoList, TodoListAdmin)    # add this to notes
admin.site.register(TodoItem, TodoItemAdmin)    # add this to notes