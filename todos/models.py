from asyncio import tasks
from tkinter import CASCADE
from django.db import models

# Create your models here.

class TodoList(models.Model):
    name = models.CharField(max_length=100)   # add this to notes
    created_on = models.DateTimeField(auto_now_add=True)    # add this to notes

    def __str__(self):       # add this to notes
        return self.name
    

class TodoItem(models.Model):
    task =  models.CharField(max_length=100)
    due_date = models.DateTimeField(auto_now=False, auto_now_add=False, null=True)  # allows field to be empty - add this to notes
    is_completed = models.BooleanField(default=False)  # by default when creating the item this is false
    list = models.ForeignKey("TodoList", related_name = "items", on_delete=models.CASCADE)  # add to notes

    def __str__(self):
        return self.task
    