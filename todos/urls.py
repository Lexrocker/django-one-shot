from django.urls import path  # might as well put this entire file into notes, it is easy to forget

from todos.views import (

    TodoListView,
    TodoDetailView,
    CreateNewListView,

)

app_name = "todos"


urlpatterns= [

    path("", TodoListView.as_view(), name="list_todos"),
    path("<int:pk>/", TodoDetailView.as_view(), name="show_todolist"),
    path("new/", CreateNewListView.as_view(), name="create_todolist"),

]
